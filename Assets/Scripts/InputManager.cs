using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    public PlayerController playerController;
    public Vector3 movementDirection;
    public Vector2 mouseDelta;

    public void OnMove(InputAction.CallbackContext context)
    {
        if (context.performed || context.canceled)
        {
            Vector2 input = context.ReadValue<Vector2>();
            movementDirection = new Vector3(input.x, movementDirection.y, input.y).normalized;
        }
    }

    public void OnCrouch(InputAction.CallbackContext context)
    {
        if(context.performed)
        {
            playerController.ToggleCrouch();
        }
    }

    public void OnSprint(InputAction.CallbackContext context)
    {
        if (context.performed || context.canceled)
        {
            playerController.ToggleSprint();
        }
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            mouseDelta = context.ReadValue<Vector2>();
            playerController.RotateCamera(mouseDelta);
        }
    }
}
