using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float maxWalkSpeed = 5f;
    public float maxRunSpeed = 20f;
    public float maxCrouchSpeed = 5f;
    public float acceleration = 1000f;
    public float standingHeight = 2f;
    public float crouchingHeight = 1.2f;
    public float crouchTransitionSpeed = 1f;
    public float drag = 1f;
    public Transform cameraParent;
    public Camera playerCamera;
    public float mouseSensitivity = 10f;
    public float minVerticalAngle = -80f;
    public float maxVerticalAngle = 80f;
    public MovementState movementState;
    public MovementStance movementStance;

    private Rigidbody _rigidbody;
    private CapsuleCollider _collider;
    private InputManager _inputManager;
    public float _maxMovementSpeed;
    private float _cameraPitch;
    private bool isTransitioning;


    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        _rigidbody = GetComponent<Rigidbody>();
        _collider = GetComponent<CapsuleCollider>();
        _inputManager = GetComponent<InputManager>();
    
        _maxMovementSpeed = maxWalkSpeed;

        cameraParent.localPosition = new Vector3(0f, standingHeight, 0f);
    }


    private void FixedUpdate()
    {
        if (_rigidbody.velocity.magnitude < 0.01f)
        {
            movementState = MovementState.IDLING;
            _maxMovementSpeed = maxWalkSpeed;
        }

        Move(_inputManager.movementDirection);
    }

    public void RotateCamera(Vector2 rotation)
    {
        transform.Rotate(Vector3.up, rotation.x * mouseSensitivity * Time.deltaTime);

        _cameraPitch -= rotation.y * mouseSensitivity * Time.deltaTime;

        _cameraPitch = Mathf.Clamp(_cameraPitch, minVerticalAngle, maxVerticalAngle);

        playerCamera.transform.localEulerAngles = Vector3.right * _cameraPitch;
    }

    public void Move(Vector3 movementVector)
    {
        Vector2 currentVelocity = new Vector2(_rigidbody.velocity.x, _rigidbody.velocity.z);
        Vector3 movementVelocity = transform.TransformDirection(movementVector * acceleration * _maxMovementSpeed * Time.fixedDeltaTime);
        
        movementVelocity.y = _rigidbody.velocity.y;


        if (currentVelocity.magnitude <= _maxMovementSpeed)
        {
            _rigidbody.velocity = movementVelocity;
        }
        if (movementVector == Vector3.zero)
        {
            _rigidbody.velocity = new Vector3(0f, _rigidbody.velocity.y, 0f);
        }
    }

    public void ToggleCrouch()
    {
        if (!isTransitioning)
        {
            if (movementStance == MovementStance.CROUCHING)
            {
                movementStance = MovementStance.STANDING;
                _maxMovementSpeed = maxWalkSpeed;
            }
            else
            {
                movementStance = MovementStance.CROUCHING;
                _maxMovementSpeed = maxCrouchSpeed;
            }

            StartCoroutine(Crouch());
        }
    }

    public void ToggleSprint()
    {
        if (!isTransitioning)
        {
            if(movementStance == MovementStance.CROUCHING)
            {
                ToggleCrouch();
                ToggleSprint();
            }
            else
            {
                if (movementState == MovementState.RUNNING)
                {
                    movementState = MovementState.WALKING;
                    _maxMovementSpeed = maxWalkSpeed;
                }
                else
                {
                    movementState = MovementState.RUNNING;
                    _maxMovementSpeed = maxRunSpeed;
                }
            }
        }
    }

    private IEnumerator Crouch()
    {
        float timeElapsed = 0f;
        float newHeight;
        isTransitioning = true;

        while (timeElapsed < crouchTransitionSpeed)
        {
            if (movementStance == MovementStance.CROUCHING)
            {
                newHeight = Mathf.Round(Mathf.Lerp(_collider.height, crouchingHeight, timeElapsed / crouchTransitionSpeed) * 10000f) / 10000f;
            }
            else
            {
                newHeight = Mathf.Round(Mathf.Lerp(_collider.height, standingHeight, timeElapsed / crouchTransitionSpeed) * 10000f) / 10000f;
            }
            cameraParent.localPosition = new Vector3(cameraParent.localPosition.x, newHeight, cameraParent.localPosition.z);
            _collider.height = newHeight;
            _collider.center = new Vector3(_collider.center.x, newHeight / 2, _collider.center.z);
            timeElapsed += Time.deltaTime;

            yield return new WaitForFixedUpdate();
        }

        isTransitioning = false;
    }
}
